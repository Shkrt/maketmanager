﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MaketManager;
using MaketManager.Models;
using System.Collections.Generic;
using System.Linq;

namespace UnitTestMGen
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GeneratesMaketFile()
        {
            MaketRecipient mr = new MaketRecipient();
            mr.AreaInn = "11";
            mr.AreaName = "TestRecp";
            mr.ConnString = @"data source=station-1\sql;initial catalog=eurodb;user id=sa;
password=qwerty;MultipleActiveResultSets=True;App=EntityFramework";
            mr.PointsList = new List<MeasuringPoint>();
            mr.SenderINN = "66677788812";
            mr.SenderName = "TestRecp";
            mr.TimeZone = "1";            

            MeasuringPoint mp1 = new MeasuringPoint();
            mp1.Name = @"TestPoint1";
            mp1.AdapterId = 28327;
            mp1.Code = "057770166601101";
            mp1.ChannelsList=new List<MeasuringChannel>();

            MeasuringChannel mc1 = new MeasuringChannel();
            mc1.Code = "01";
            mc1.Multiplier = 320;
            mc1.Name = @"TestChannel_1-1";
            mc1.ParamId = 1035761;
            mc1.ValuesByDay.Add(Enumerable.Range(10,48).ToList());

            mp1.ChannelsList.Add(mc1);

            MeasuringPoint mp2 = new MeasuringPoint();
            mp2.Name = @"TestPoint1";
            mp2.AdapterId = 28334;
            mp2.Code = "057770166601102";
            mp2.ChannelsList = new List<MeasuringChannel>();

            MeasuringChannel mc2 = new MeasuringChannel();
            mc2.Code = "01";
            mc2.Multiplier = 320;
            mc2.Name = @"TestChannel_2-1";
            mc2.ParamId = 1035881;
            mc2.ValuesByDay.Add(Enumerable.Range(110, 48).ToList());

            mp2.ChannelsList.Add(mc2);

            mr.PointsList.Add(mp1);
            mr.PointsList.Add(mp2);

            string fName = Tool.OutOneFile(mr, "20150423", "14623", 0, @"C:\\Qoobox\2");
            Assert.AreEqual("80020_11_20150423_14623.xml", fName);
        }
    }
}
