﻿using System;
using System.Collections.Generic;
using System.Linq;
using MaketManager.Models;
using System.Xml.Linq;
using System.Xml;
using System.Text;
using System.IO;
using System.Globalization;


namespace MaketManager
{
    public class Tool
    {
        static string[] hfNames = new string[] {"0000","0030","0030","0100","0100","0130","0130","0200","0200","0230","0230","0300",
                "0300","0330","0330","0400","0400","0430","0430","0500","0500","0530","0530","0600","0600","0630","0630","0700","0700",
            "0730","0730","0800","0800","0830","0830","0900","0900","0930","0930","1000","1000","1030","1030","1100","1100","1130",
            "1130","1200","1200","1230","1230","1300","1300","1330","1330","1400","1400","1430","1430","1500","1500","1530","1530",
            "1600","1600","1630","1630","1700","1700","1730","1730","1800","1800","1830","1830","1900","1900","1930","1930","2000",
            "2000","2030","2030","2100","2100","2130","2130","2200","2200","2230","2230","2300","2300","2330","2330","0000"};
        static string[] hfTemplate = new string[] {"0030","0100","0130","0200","0230","0300","0330","0400","0430","0500",
            "0530","0600","0630","0700", "0730","0800","0830","0900","0930","1000","1030","1100","1130","1200","1230","1300","1330",
            "1400","1430","1500","1530","1600","1630","1700","1730","1800","1830","1900","1930","2000","2030","2100","2130","2200",
            "2230","2300","2330","0000"};
        const int hfInDay = 48;

        public static MaketRecipient ParseConfig(string file)
        {//обрабатывает один конфигурационный файл, создает один объект MaketRecipient
            try
            {
                XmlTextReader reader = new XmlTextReader(file);
                reader.Normalization = false;
                XDocument doc = XDocument.Load(reader);
                StringBuilder sb = new StringBuilder();                
                MaketRecipient mrc = new MaketRecipient();
                
                if (doc.Root.Attribute("COM").Value != "SQL_Stg.MSSQLStorage" || doc.Root.Attribute("type").Value != "80020")
                    return null;

                string[] sep = { "\r\n" };
                string tosplit = doc.Root.Attribute("settings").Value;
                string[] s = tosplit.Split(sep, StringSplitOptions.None);
                Char[] spl = new Char[] { '=' };

                sb.Append("data source=").Append(s[1].Split(spl)[1].Trim()).
                    Append(";initial catalog=").Append(s[3].Split(spl)[1].Trim()).
                    Append(";user id=").Append(s[4].Split(spl)[1].Trim()).
                    Append(";password=qwerty").Append(";MultipleActiveResultSets=True;App=EntityFramework");

                mrc.ConnString = sb.ToString();
                mrc.SenderINN = doc.Root.Attribute("Inn").Value;
                mrc.AreaInn = doc.Root.Attribute("InnAtc").Value;
                mrc.AreaName = doc.Root.Attribute("OrgName").Value;
                mrc.SenderName = doc.Root.Attribute("Sender").Value;
                mrc.TimeZone = doc.Root.Attribute("TimeZone").Value;
                XAttribute contract = doc.Root.Attribute("Contract");

                if (contract!=null)
                {
                    mrc.ContractID = doc.Root.Attribute("Contract").Value;                    
                }                

                foreach (XElement elem in doc.Root.Elements())
                {
                    if (elem.Name == "device")
                    {
                        foreach (XElement el in elem.Elements())
                        {
                            if (el.Name == "parameter_group")
                            {
                                MeasuringPoint mPoint = new MeasuringPoint();
                                mPoint.Code = el.Attribute("uid").Value;
                                mPoint.Name = el.Attribute("name").Value;
                                mPoint.AdapterId = Convert.ToInt32(el.Attribute("id").Value);

                                foreach (XElement item in el.Elements())
                                {
                                    if (item.Name == "parameter")
                                    {
                                        MeasuringChannel mChan = new MeasuringChannel();
                                        mChan.Code = item.Attribute("uid").Value;
                                        mChan.Multiplier = Convert.ToDouble(item.Attribute("MeasureKoeff").Value)/2;
                                        mChan.Name = item.Attribute("name").Value;
                                        mChan.ParamId = Convert.ToInt32(item.Attribute("id").Value);
                                        mPoint.ChannelsList.Add(mChan);
                                    }
                                }
                                mrc.PointsList.Add(mPoint);
                            }
                        }
                    }
                }
                return mrc;
            }

            catch (Exception)
            {
                return null;
            }
        }

        public static List<Tuple<DateTime,DateTime>> PrepareDates(DateTime first, DateTime last)//возвращает список дат за заданный месяц
        {
            List<Tuple<DateTime, DateTime>> result = new List<Tuple<DateTime, DateTime>>();
            int dayCount = (last - first).Days;
            
            for (int i = 0; i < dayCount; i++)
            {
                result.Add(new Tuple<DateTime,DateTime> (first,first.AddDays(1)));
                first = first.AddDays(1);
            }

            return result;
        }

        public static double CalculateCompleteness(DateTime minRecTime, DateTime maxRecTime, int adId, int criteria,string cString)
        {//Подсчитывает процент сбора данных, criteria в нашем случае равна 48
            double count = 0;
            using (GenericEntities ge = new GenericEntities())
            {
                ge.Database.Connection.ConnectionString = cString;
                List<int> idRec = ge.records.
                    Where(x => x.RECORD_TIME >= minRecTime && x.RECORD_TIME < maxRecTime && x.ID_ADAPTER == adId).
                    Select(y => y.ID_RECORD).ToList();
                count = idRec.Count();
            }
            return Math.Round(count / criteria * 100,2);
        }

        public static List<int> GetValues(string cString, int idParam, DateTime minRecTime, DateTime maxRecTime, double mult, int idAdapter)
        {  
            Dictionary<DateTime, double> resDict = new Dictionary<DateTime, double>();
            using (GenericEntities ge = new GenericEntities())
            {
                ge.Database.Connection.ConnectionString = cString;
                ge.Database.CommandTimeout = 300;
                var tempResult = ge.data.Join(ge.records, p => p.ID_RECORD, q => q.ID_RECORD, (p, q) => new { p, q }).
                    Where(r =>
                        r.p.ID_PARAMETER == idParam &&
                        r.q.RECORD_TIME > minRecTime &&
                        r.q.RECORD_TIME <= maxRecTime &&
                        r.q.ID_ADAPTER == idAdapter).
                    Select(r => r);
                    //.ToDictionary(k => k.q.RECORD_TIME, v => v.p.MEASURE_VALUE);

                foreach (var item in tempResult)
                {
                    if (!resDict.ContainsKey(item.q.RECORD_TIME))
                    {
                        resDict.Add(item.q.RECORD_TIME, item.p.MEASURE_VALUE);
                    }
                }
            }
            
            List<string> timeList = resDict.Keys.Select(x => x.ToString("HHmm")).ToList();
            if (timeList.SequenceEqual(hfTemplate))
            {               
                return RoundList(resDict.Values.Select(x => x * mult).ToList());
            }

            else
            {
                Dictionary<string, double> tempDict = resDict.Select(x => x).ToDictionary(x => x.Key.ToString("HHmm"), x => x.Value);               
                return RoundList((FixSequence(tempDict)).Values.Select(x => x * mult).ToList());
            }
        }

        public static string OutOneFile(MaketRecipient outp, string dayName, string fileNum, int dayIndex, string outDir)
        {//генерирует один файл макета по готовым данным
            string generateTime = DateTime.Now.ToString("yyyyMMddHHmmss");//строка для xml-файла                        
            string fileName = String.Format("{3}\\80020_{0}_{1}_{2}.xml", outp.AreaInn, dayName, fileNum,outDir);//строка для xml-файла
            
            XElement area = new XElement("area",
                new XAttribute("timezone", outp.TimeZone),
                new XElement("inn", outp.AreaInn),
                new XElement("name", outp.AreaName));
            XDocument doc = new XDocument(
                new XDeclaration("1.0", "windows-1251", ""),
                new XElement("message",
                    new XAttribute("class", "80020"),
                    new XAttribute("version", "2"),
                    new XAttribute("number", fileNum),
                    new XElement("datetime",
                        new XElement("timestamp", generateTime),
                        new XElement("daylightsavingtime", outp.TimeZone),
                        new XElement("day", dayName)
                        ),
                    new XElement("sender",
                        new XElement("inn", outp.SenderINN),
                        new XElement("name", outp.SenderName)),
                    area
                                        ));

            foreach (var item in outp.PointsList)
            {
                XElement point = new XElement("measuringpoint", new XAttribute("code", item.Code), new XAttribute("name", item.Name));
                foreach (var chan in item.ChannelsList)
                {
                    XElement channel = new XElement("measuringchannel", new XAttribute("code", chan.Code), new XAttribute("desc",chan.Name));
                    int i=0;
                    foreach (double val in chan.ValuesByDay[dayIndex])
                    {
                        XElement period = new XElement("period", new XAttribute("start",hfNames[i]),new XAttribute("end",hfNames[i+1]),
                            new XElement("value",val));                        
                        channel.Add(period);
                        i+=2;
                    }
                    point.Add(channel);
                }
                area.Add(point);
            }
            doc.Save(fileName);
            return fileName;
        }

        private static Dictionary<string,double> FixSequence(Dictionary<string,double> input)
        {//this method fixes input dictionary if data is incomplete
            //finding incorrect keys and "rounding" them
            Dictionary<string, double> tempDict = new Dictionary<string, double>();
            List<string> incorrectKeys = new List<string>();
            foreach (KeyValuePair<string,double> item in input)
            {
                if (!hfNames.Contains(item.Key))
                {
                    DateTime d1 = DateTime.ParseExact(item.Key, "HHmm", CultureInfo.InvariantCulture);
                    tempDict.Add(RoundDate(d1).ToString("HHmm"), item.Value);
                    incorrectKeys.Add(item.Key);
                }
            }

            //CONCATENATING TWO DICTIONARIES
            //if the rounding gives the same key, the key is just thrown away
            input = input.Concat(tempDict.Where(kvp=>!input.ContainsKey(kvp.Key))).ToDictionary(x => x.Key, x => x.Value);

            //removing incorrect entries, that were alreadey replaced by "rounded"
            foreach (string key in incorrectKeys)
            {
                if (input.ContainsKey(key))
                {
                    input.Remove(key);
                }
            }

            //determining keys that are not present in input dictionary
            List<string> notPresentKeys = new List<string>();
            for (int j = 0; j < hfNames.Length; j+=2)
            {
                if (!input.Keys.Contains(hfNames[j]))
                {
                    notPresentKeys.Add(hfNames[j]);
                }                
            }
            //ADDING ABSENT KEYS TO DICTIONARY
            foreach (string item in notPresentKeys)
            {
                input.Add(item, 0);
            }

            //SORTING DICTIONARY
            Dictionary<string, double> res = input.OrderBy(x => x.Key).ToDictionary(y=>y.Key,y=>y.Value);
            return res;            
        }

        private static DateTime RoundDate(DateTime dateTime)
        {
            var hour = dateTime.Hour;
            var updated = dateTime.AddMinutes(30);
            if (updated.Hour==hour)
            {
                return new DateTime(updated.Year, updated.Month, updated.Day,
                                 updated.Hour, 0, 0, dateTime.Kind);
            }

            else
            {
                return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day,
                                 dateTime.Hour, 30, 0, dateTime.Kind);
            }
        }

        public static void TestCase()
        {
            string output = System.Web.Configuration.WebConfigurationManager.AppSettings["outputDir"];
            List<MaketRecipient> mrList = _GetDirs();
            DateTime d1 = new DateTime();
            DateTime d2 = new DateTime();
            DateTime.TryParse("2015-04-01 00:00:00.000", out d1);
            DateTime.TryParse("2015-05-01 00:00:00.000", out d2);
            foreach (MaketRecipient mrc in mrList)
            {
                string dirName = String.Format("{0}\\{1}", output, mrc.Performer);
                _MakeOutput(d1, d2, mrc, dirName);

            }
        }

        public static List<MaketRecipient> _GetDirs()
        {//returns ... and creates output dirs for groups if they don't exist
            string root = System.Web.Configuration.WebConfigurationManager.AppSettings["confDir"];
            string output = System.Web.Configuration.WebConfigurationManager.AppSettings["outputDir"];
            List<MaketRecipient> mrcList = new List<MaketRecipient>();

            //LOOKING INSIDE ROOT DIRECTORY
            string[] rootGroup = Directory.GetFiles(root);

            int maxId = 0;

            foreach (string item in rootGroup)
            {
                MaketRecipient m = ParseConfig(item);
                m.Performer = "";
                mrcList.Add(m);
                maxId++;
                m.Id = maxId;
            }

            //LOOKING INSIDE CHILD DIRECTORIES, JUST FIRST LEVEL
            foreach (string srcDir in Directory.GetDirectories(root))
            {
                string perfName = srcDir.Remove(0, root.Length + 1);
                string destDir = String.Format("{0}{1}", output, srcDir.Remove(0, root.Length));

                foreach (string fileName in Directory.GetFiles(srcDir))
                {
                    MaketRecipient m = ParseConfig(fileName);
                    m.Performer = perfName;
                    mrcList.Add(m);
                    maxId++;
                    m.Id = maxId;
                }

                if (!Directory.Exists(destDir))
                {
                    try
                    {
                        Directory.CreateDirectory(destDir);
                    }
                    catch (Exception)
                    {
                        
                    }
                }                
            }
            return mrcList;
        }

        public static void _MakeOutput(DateTime d1, DateTime d2,MaketRecipient mr, string outputDirectory)
        {//makes output for one conf file for one time period            
            List<Tuple<DateTime, DateTime>> dateRange = GetDatesFromRange(d1, d2);
            StringBuilder sb = new StringBuilder();
            string outDir=sb.Append(outputDirectory).Append("\\").Append(mr.SenderINN).Append("_").
                Append(mr.ContractID).Append("_").Append(d1.ToString("MM")).Append("_").
                Append(d2.ToString("yy")).ToString();
            if (!Directory.Exists(outDir))
            {
                Directory.CreateDirectory(outDir);                
            }
            //
            //get rid of nested foreaches
            //            
            for (int i = 0; i < dateRange.Count; i++)
            {                
                foreach (var point in mr.PointsList)
                {
                    foreach (var chan in point.ChannelsList)
                    {
                        chan.ValuesByDay.Add(GetValues(mr.ConnString, chan.ParamId, dateRange[i].Item1, dateRange[i].Item2, chan.Multiplier,point.AdapterId));
                    }
                }
                OutOneFile(mr, dateRange[i].Item1.Date.ToString("yyyyMMdd"), (i + 1).ToString(), i, outDir);
            }
        }

        private static void ExampleOfMaketGen(string outputDirectory)
        {
            //this should be got from input
            string file = @"C:\Qoobox\1\++Yarmarka.xml";            
            DateTime d1 = new DateTime();
            DateTime d2 = new DateTime();
            DateTime.TryParse("2015-04-01 00:00:00.000", out d1);
            DateTime.TryParse("2015-05-01 00:00:00.000", out d2);
            //
            MaketRecipient mr = ParseConfig(file);
            List<Tuple<DateTime, DateTime>> dateRange = GetDatesFromRange(d1, d2);
            //
            //get rid of nested foreaches
            //            
            for (int i = 0; i < dateRange.Count; i++)
            {
                foreach (var point in mr.PointsList)
                {
                    foreach (var chan in point.ChannelsList)
                    {
                        chan.ValuesByDay.Add(GetValues(mr.ConnString, chan.ParamId, dateRange[i].Item1, dateRange[i].Item2, chan.Multiplier,point.AdapterId));
                    }
                }
                OutOneFile(mr, dateRange[i].Item1.Date.ToString("yyyyMMdd"), "01", i, outputDirectory);
            }
            Console.Write("ok"); 
        }

        //public static void TestCase()
        //{
        //    string file = @"C:\Qoobox\1\++Yarmarka.xml";
        //    //string cString = @"data source=STATION-1\SQL;initial catalog=EuroDB;user id=sa;password=qwerty;MultipleActiveResultSets=True;App=EntityFramework";
        //    DateTime d1 = new DateTime();
        //    DateTime d2 = new DateTime();
        //    DateTime.TryParse("2015-04-01 00:00:00.000", out d1);
        //    DateTime.TryParse("2015-05-01 00:00:00.000", out d2);
        //    MaketRecipient mr = ParseConfig(file);
        //    List<Tuple<DateTime, DateTime>> dateRange = GetDatesFromRange(d1, d2);
        //    //
        //    //get rid of nested foreaches
        //    //            
        //    for (int i = 0; i < dateRange.Count; i++)
        //    {
        //        foreach (var point in mr.PointsList)
        //        {
        //            foreach (var chan in point.ChannelsList)
        //            {
        //                chan.ValuesByDay.Add(GetValues(mr.ConnString, chan.ParamId, dateRange[i].Item1, dateRange[i].Item2, chan.Multiplier));
        //            }
        //        }
        //        GenerateOutput(mr, dateRange[i].Item1.Date.ToString("yyyyMMdd"), "01", i);
        //    }
        //    Console.Write("ok");
        //}

        private static List<int> RoundList(List<double> input)
        {
            List<int> result = new List<int>();
            double remainder = 0;
            foreach (var item in input)
            {
                double r = item + remainder;
                double next = Math.Round(r,MidpointRounding.AwayFromZero);
                remainder = r-next;
                result.Add(Convert.ToInt32(next));
            }

            return result;
        }

        private static List<Tuple<DateTime, DateTime>> GetDatesFromRange(DateTime d1, DateTime d2)
        {//input - range of dates, output - List<T> of ranges, splitted by day
            List<Tuple<DateTime, DateTime>> result = new List<Tuple<DateTime, DateTime>>();
            DateTime fDate = d1;
            for (DateTime date = d1; date < d2; date = date.AddDays(1))
            {
                DateTime newDate = date.AddDays(1);
                result.Add(new Tuple<DateTime, DateTime>(fDate, newDate ));
                fDate = newDate;
            }
            return result;
        }
    }
}