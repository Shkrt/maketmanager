﻿using System.Collections.Generic;

namespace MaketManager
{
    class Accumulated
    {
        public List<Counter> Counters { get; set; }
        public DBSettings DBconnection { get; set; }
        public List<string> MailRecipients { get; set; }
        public string EmailSubject { get; set; }
        public RecipientData Info { get; set; }

        public Accumulated()
        {
            this.Counters = new List<Counter>();
            this.Info = new RecipientData();
            this.DBconnection = new DBSettings();
            this.MailRecipients = new List<string>();
        }
    }

    class Counter
    {
        public string Name { get; set; }        
        public int AdapterId { get; set; }
        public int ParameterId { get; set; }
        public double Divider { get; set; }
        public bool Ignore { get; set; }
        public string UniqueId { get; set; }

        public Counter(string name, int adid, int parid, double divider,string uid)
        {
            this.Name = name;            
            this.AdapterId = adid;
            this.ParameterId = parid;
            this.Divider = divider;
            this.Ignore = false;
            this.UniqueId = "null";
        }

        public Counter()
        {}        
    }

    class DBSettings
    {
        public string ServerName { get; set; }
        public string DatabaseName { get; set; }
        public int AuthenticationType { get; set; }
        public string LoginName { get; set; }
        public string Password { get; set; }
    }

    class RecipientData
    {
        public string Name { get; set; }
        public string Contract { get; set; }
    }
    
}
