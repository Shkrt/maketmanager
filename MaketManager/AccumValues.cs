﻿using MaketManager.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace MaketManager
{
    public class AccumValues
    {
        
        internal static Tuple<double,double> GetAccumulatedValue(DateTime date, string contract, string uid)
        {            
            double result = double.NaN;
            
            List<string> msFiles = FindByContract(contract);
            Accumulated acc = null;
            Counter oneCounter = null;
            foreach (string item in msFiles)
            {
                acc = ParseMsFile(item);
                oneCounter = acc.Counters.Where(x => x.UniqueId == uid).FirstOrDefault();
                if (oneCounter!=null)
                {
                    break;
                }
            }

            if (acc == null)
            {
                StringBuilder stb = new StringBuilder();
                string message = stb.Append("CMS file was not found for contract with id = ").
                    Append(contract).ToString();
                throw new CMSFileNotFoundException(message);                
            }

            if (oneCounter == null)
            {
                StringBuilder stb = new StringBuilder();
                string message = stb.Append("counter with code = ").
                    Append(uid).Append(" was not found in the CMS file").ToString();
                throw new CodeNotFoundException(message);
            }

            double divider = oneCounter.Divider;
            
            StringBuilder sb = new StringBuilder();
            sb.Append("data source=").Append(acc.DBconnection.ServerName).
                    Append(";initial catalog=").Append(acc.DBconnection.DatabaseName).
                    Append(";user id=").Append(acc.DBconnection.LoginName).
                    Append(";password=").Append(acc.DBconnection.Password)
                    .Append(";MultipleActiveResultSets=True;App=EntityFramework");
            string conString = sb.ToString();
            using (GenericEntities ge = new GenericEntities())
            {
                ge.Database.Connection.ConnectionString = conString;
                ge.Database.CommandTimeout = 300;
                int idRec = ge.records.Where(x => x.RECORD_TIME == date && x.ID_ADAPTER==oneCounter.AdapterId).Select(x => x.ID_RECORD).FirstOrDefault();
                var tempResult = ge.data.Where(x => x.ID_PARAMETER == oneCounter.ParameterId && 
                    x.ID_RECORD == idRec).Select(f => f.MEASURE_VALUE).FirstOrDefault();
                double tempRes = Convert.ToDouble(tempResult);
                result=tempRes;

                if (oneCounter.Divider!=0)
                {
                    result = tempRes / oneCounter.Divider;
                }
            }
            return new Tuple<double, double> (result,divider);
        }

        private static Accumulated ParseMsFile(string file)
        {
            try
            {
                XmlTextReader reader = new XmlTextReader(file);
                reader.Normalization = false;
                XDocument doc = XDocument.Load(reader);
                StringBuilder sb = new StringBuilder();
                Accumulated acc = new Accumulated();
                
                foreach (XElement elem in doc.Root.Elements())
                {
                    if (elem.Name == "counters")
                    {
                        foreach (XElement el in elem.Elements())
                        {
                            if (el.Name == "counter")
                            {
                                Counter cnt = new Counter();
                                cnt.AdapterId = Convert.ToInt32(el.Attribute("idAdapter").Value);
                                cnt.Divider = Convert.ToInt32(el.Attribute("divider").Value);
                                cnt.Ignore = Convert.ToBoolean(el.Attribute("ignore").Value);
                                cnt.Name = el.Attribute("name").Value;
                                cnt.ParameterId = Convert.ToInt32(el.Attribute("idParameter").Value);
                                cnt.UniqueId = el.Attribute("uid").Value;
                                acc.Counters.Add(cnt);
                            }                            
                        }
                    }

                    if (elem.Name == "dbsettings")
                    {
                        DBSettings dbs = new DBSettings();
                        dbs.ServerName = elem.Attribute("servername").Value;
                        dbs.AuthenticationType = Convert.ToInt32(elem.Attribute("authtype").Value);
                        dbs.DatabaseName = elem.Attribute("dbname").Value;
                        dbs.LoginName = elem.Attribute("login").Value;
                        dbs.Password = elem.Attribute("pass").Value;
                        acc.DBconnection = dbs;
                    }

                    if (elem.Name == "mailrecipients")
                    {
                        foreach (XElement elm in elem.Elements())
                        {
                            if (elm.Name == "email")
                            {
                                acc.MailRecipients.Add(elm.Value);
                            }
                        }
                    }

                    if (elem.Name == "subject")
                    {
                        acc.EmailSubject = elem.Value;
                    }

                    if (elem.Name == "recipientdata")
                    {
                        RecipientData rData = new RecipientData();
                        foreach (XElement child in elem.Elements())
                        {
                            rData.Contract = child.Attribute("contract").Value;
                            rData.Name = child.Attribute("name").Value;
                        }
                    }
                }
                return acc;
            }

            catch (Exception)
            {
                return null;
            }
        }

        private static List<string> FindByContract(string contract)
        {   
            string msConfDir = System.Web.Configuration.WebConfigurationManager.AppSettings["msConfDir"];
            string[] files = Directory.GetFiles(msConfDir);
            List<string> result = new List<string>();

            foreach (string file in files)
            {
                try
                {   
                    XDocument doc = XDocument.Load(file);
                    string contractName = doc.Root.Elements().
                        Where(x => x.Name == "recipientdata").
                        Select(x => x.Attribute("contract").Value).
                        FirstOrDefault();

                    if (contractName == contract)
                    {
                        result.Add(file);
                    }
                }

                catch (Exception)
                {
                    continue;
                }                
            }
            return result;
        }
    }
}