﻿using MaketManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MaketManager.Controllers
{
    public class ChoiceController : Controller
    {
        //
        // GET: /Choice/

        [HttpPost]
        public ActionResult Output(MaketModel mm, string outputgen, string mailsend)
        {
            if (!string.IsNullOrEmpty(outputgen))
            {
                List<int> chosen = mm.Objects.Where(x => x.CheckedStatus == true).Select(z => z.Id).ToList();
                Models.MaketModel chsn = Session["FullMod"] as Models.MaketModel;
                Models.MaketModel mRes = new MaketModel();
                mRes.Objects = chsn.Objects.Select(x => x).Where(y => chosen.Contains(y.Id)).ToList();
                Tuple<DateTime, DateTime> daterange = Session["DateRange"] as Tuple<DateTime, DateTime>;

                string output = System.Web.Configuration.WebConfigurationManager.AppSettings["outputDir"];

                foreach (MaketRecipient mrc in mRes.Objects)
                {
                    string dirName = String.Format("{0}\\{1}", output, mrc.Performer);
                    Tool._MakeOutput(daterange.Item1, daterange.Item2, mrc, dirName);
                }

                return View();                
            }

            else
            {
                //determining selected objects and dates
                List<int> chosen = mm.Objects.Where(x => x.CheckedStatus == true).Select(z => z.Id).ToList();
                Models.MaketModel chsn = Session["FullMod"] as Models.MaketModel;
                Models.MaketModel mRes = new MaketModel();
                mRes.Objects = chsn.Objects.Select(x => x).Where(y => chosen.Contains(y.Id)).ToList();
                Tuple<DateTime, DateTime> daterange = Session["DateRange"] as Tuple<DateTime, DateTime>;

                foreach (MaketRecipient mrc in mRes.Objects)
                {
                    Mailsend.Send(mrc);
                }

                return View();
            }
            
        }

    }
}
