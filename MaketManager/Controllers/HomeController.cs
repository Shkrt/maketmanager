﻿using MaketManager.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using MaketManager;

namespace BootstrapMvcSample.Controllers
{
    public class HomeController : BootstrapBaseController
    {
        public ActionResult Index()
        {
            List<SelectListItem> months = new List<SelectListItem> ();
            months.Add(new SelectListItem{Text="Январь",Value="1"});
            months.Add(new SelectListItem{Text="Февраль",Value="2"});
            months.Add(new SelectListItem{Text="Март",Value="3"});
            months.Add(new SelectListItem{Text="Апрель",Value="4"});
            months.Add(new SelectListItem{Text="Май",Value="5"});
            months.Add(new SelectListItem{Text="Июнь",Value="6"});
            months.Add(new SelectListItem{Text="Июль",Value="7"});
            months.Add(new SelectListItem{Text="Август",Value="8"});
            months.Add(new SelectListItem{Text="Сентябрь",Value="9"});
            months.Add(new SelectListItem{Text="Октябрь",Value="10"});
            months.Add(new SelectListItem{Text="Ноябрь",Value="11"});
            months.Add(new SelectListItem{Text="Декабрь",Value="12"});

            List<SelectListItem> years = new List<SelectListItem>();
            years.Add(new SelectListItem { Text = "2012", Value = "2012" });
            years.Add(new SelectListItem { Text = "2013", Value = "2013" });
            years.Add(new SelectListItem { Text = "2014", Value = "2014" });
            years.Add(new SelectListItem { Text = "2015", Value = "2015" });
            
            ViewBag.Month = months;
            ViewBag.Year = years;

            //Tool.TestCase();

            return View();
        }

        public ActionResult GetConfsList(string month, string year)
        {
            List<MaketRecipient> recipients = Tool._GetDirs(); 

            DateTime currentMonth = new DateTime();//начальная дата текущего месяца            
            DateTime.TryParse(month + "." + year, out currentMonth);
            DateTime nextMonth = currentMonth.AddMonths(1);//начальная дата следующего месяца
            
            List<Tuple<DateTime,DateTime>> days =Tool.PrepareDates(currentMonth, nextMonth); //список дней 
            

            //вычисление процента заполнения БД
            recipients.Select(x =>
            {                
                x.PointsList                    
                    .Select(y => 
                    {
                        Dictionary<string, double> cbd = new Dictionary<string, double>();
                        foreach (Tuple<DateTime,DateTime> pair in days)
                        {
                            double comp=Tool.CalculateCompleteness(pair.Item1, pair.Item2, y.AdapterId, 48,x.ConnString);
                            cbd.Add(pair.Item1.Day.ToString(),comp);
                        }
                        y.CompletenessByDay = cbd;
                        List<string> incompleteList = y.CompletenessByDay.Where(c => c.Value != 100.0).Select(c => c.Key).ToList();
                        y.CompByDayString = string.Join(", ",incompleteList);
                        y.OverallCompleteness = (int)y.CompletenessByDay.Values.Sum(val => val) / days.Count;
                        return y;
                    }).ToList();
                return x;
            }).ToList();

            recipients.Select(x =>
            {
                x.PointsList
                    .Select(y =>
                    {
                        y.ChannelsList
                            .Select(z =>
                            {
                                z.FirstValue = AccumValues.GetAccumulatedValue(currentMonth, x.ContractID, y.Code);
                                z.LastValue = AccumValues.GetAccumulatedValue(nextMonth, x.ContractID, y.Code);
                                if (z.FirstValue.Item2==z.LastValue.Item2)
                                {
                                    z.ConsumedAmount = Math.Round((z.Multiplier * z.LastValue.Item2 * (z.LastValue.Item1 - z.FirstValue.Item1)),4);    
                                }                                
                                return z;
                            }).ToList();
                        return y;
                    }).ToList();
                return x;
            }).ToList();

            
            MaketModel mm = new MaketModel();            
            mm.Objects = recipients;
            Session["FullMod"] = mm;
            Session["DateRange"] = new Tuple<DateTime,DateTime> (currentMonth,nextMonth);
            return View(mm);
        }

        

        

        

        

        
    }
}
