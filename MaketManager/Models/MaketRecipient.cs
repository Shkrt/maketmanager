﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MaketManager.Models
{
    
    public class MaketModel
    {        
        public List<MaketRecipient> Objects { get; set; }
    }
    

    public class MaketRecipient
    {   
        public string AreaInn { get; set; }
        public string AreaName { get; set; }
        public string TimeZone { get; set; }
        public string SenderINN { get; set; }
        public string SenderName { get; set; }        
        public List<MeasuringPoint> PointsList { get;set;}
        public string ConnString { get; set; }
        public string ContractID { get; set; }
        public string Performer { get; set; }
        //Experimental code
        public bool CheckedStatus { get; set; }
        public int Id { get; set; }  //<--this should be tracked properly
        //

        public MaketRecipient()
        {
            this.PointsList = new List<MeasuringPoint>();                    
        }
    }

    public class MeasuringChannel
    {
        public double Multiplier { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int ParamId { get; set; }
        public List<List<int>> ValuesByDay { get; set; }
        public Tuple<double,double> FirstValue { get; set; } //показания на начало периода
        public Tuple<double,double> LastValue { get; set; } //показания на конец периода
        public double ConsumedAmount { get; set; }

        public MeasuringChannel()
        {
            this.ValuesByDay = new List<List<int>>();            
        }
    }
    
    public class MeasuringPoint
    {
        public string Name { get; set; }
        public string Code { get; set; }   
        public List<MeasuringChannel> ChannelsList {get;set;}
        public Dictionary<string, double> CompletenessByDay { get; set; }
        public int OverallCompleteness { get; set; }
        public int AdapterId { get; set; }
        public string CompByDayString { get; set; }

        public MeasuringPoint()
        {
            this.ChannelsList = new List<MeasuringChannel>();
        }
    }
}