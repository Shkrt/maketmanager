﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MaketManager.Models
{
    public class Dates
    {
        //public int Day { get; set; }
        //public int Month { get; set; }
        //public int Year { get; set; }
        public List<string> Month { get; set; }

        public Dates()
        {
            this.Month = new List<string> { "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" };
        }
    }
}