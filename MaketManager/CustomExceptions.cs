﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MaketManager
{
    public class CMSFileNotFoundException : Exception
    {
        public CMSFileNotFoundException(string message)
            :base(message)
        {

        }
    }

    public class CodeNotFoundException : Exception
    {
        public CodeNotFoundException(string message)
            : base(message)
        {

        }
    }
}