//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MaketManager
{
    using System;
    using System.Collections.Generic;

    public partial class versions
    {
        public int ID_VERSION { get; set; }
        public Nullable<float> VER { get; set; }
        public Nullable<System.DateTime> VER_DATE { get; set; }
        public string CHANGES { get; set; }
    }
}
